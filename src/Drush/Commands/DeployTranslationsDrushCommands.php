<?php

namespace DeployTranslations\Drush\Commands;

use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Consolidation\SiteAlias\SiteAliasManagerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Drush\Commands\core\LocaleCommands as DrushLocaleCommands;
use Drush\Drush;

/**
 * A Drush commandfile.
 */
final class DeployTranslationsDrushCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * {@inheritdoc}.
   */
  public function __construct(
    private readonly SiteAliasManagerInterface $siteAliasManager
  ) {
    parent::__construct();
  }

  #[CLI\Hook(type: HookManager::POST_COMMAND_HOOK, target: 'updatedb')]
  #[CLI\ValidateModulesEnabled(modules: ['locale'])]
  public function deployTranslations(int $result): void {
    if ($result !== 0) {
      return;
    }
    $self = $this->siteAliasManager->getSelf();
    $redispatchOptions = Drush::redispatchOptions();
    // @var \Drush\SiteAlias\ProcessManager $manager
    $manager = $this->processManager();

    $this->logger()->notice("Locale updates start.");

    $process = $manager->drush($self, DrushLocaleCommands::CHECK, [], $redispatchOptions);
    $process->mustRun($process->showRealtime());

    $process = $manager->drush($self, DrushLocaleCommands::UPDATE, [], $redispatchOptions);
    $process->mustRun($process->showRealtime());

    if (!is_dir('../translations')) {
      return;
    }

    $process = $manager->drush($self, DrushLocaleCommands::IMPORT_ALL, ['../translations'], $redispatchOptions + ['type' => 'customized', 'override' => 'all']);
    $process->mustRun($process->showRealtime());
  }

}
