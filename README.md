# Drush Deploy Translations

## Getting started

Require this package with composer. After that any `drush updatedb` (including `drush deploy`) will update the translations.

## Usage

It supports custom translations in a `translations` directory above the Drupal root.
For example if you have a `/translations/es.po` file that file will be imported into the database.
